# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
# append history
setopt appendhistory
setopt HIST_IGNORE_DUPS
# unset bees sount
unsetopt beep
bindkey -v
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/bakablue/.zshrc'

autoload -Uz compinit promptinit colors select-word-style
# auto completion
compinit
# auto-completion with arrow-key interface
zstyle ':completion:*' menu select
# auto-completion for aliases
setopt completealiases
promptinit      # prompt
colors          # colors
select-word-style bash # ctrl+w on words

# default prompt
PS1="%{$fg[green]%}%n%{$reset_color%}@%{$fg[blue]%}%m %{$fg[yellow]%}%~ %{$reset_color%}%% "

# spectial keys
typeset -A key

key[Home]=${terminfo[khome]}
key[End]=${terminfo[kend]}
key[Insert]=${terminfo[kich1]}
key[Delete]=${terminfo[kdch1]}
key[Up]=${terminfo[kcuu1]}
key[Down]=${terminfo[kcud1]}
key[Left]=${terminfo[kcub1]}
key[Right]=${terminfo[kcuf1]}
key[PageUp]=${terminfo[kpp]}
key[PageDown]=${terminfo[knp]}

# setup key
[[ -n "${key[Home]}" ]] && bindkey "${key[Home]}"           beginning-of-line
[[ -n "${key[End]}" ]] && bindkey "${key[End]}"             end-of-line
[[ -n "${key[Insert]}" ]] && bindkey "${key[Insert]}"       overwrite-mode
[[ -n "${key[Delete]}" ]] && bindkey "${key[Delete]}"       delete-char
[[ -n "${key[Up]}" ]] && bindkey "${key[Up]}"               up-line-or-history
[[ -n "${key[Down]}" ]] && bindkey "${key[Down]}"           down-line-or-history
[[ -n "${key[Left]}" ]] && bindkey "${key[Left]}"           backward-char
[[ -n "${key[Right]}" ]] && bindkey "${key[Right]}"         forward-char
[[ -n "${key[PageUp]}" ]] && bindkey "${key[PageUp]}"       beginning-of-buffer-or-history
[[ -n "${key[PageDown]}" ]] && bindkey "${key[PageDown]}"   end-of-buffer-or-history

bindkey '^a' beginning-of-line # bind command to a keybinding
bindkey '^e' end-of-line # bind command to a keybinding
bindkey '^r' history-incremental-pattern-search-backward
bindkey '^k' kill-line

# if terminal is in application mode,
# zle active
if (( ${+terminfo[smkx]} )) && (( ${+terminfo[rmkx]} )); then
    function zle-line-init ()
    {
        printf '%s' "${terminfo[smkx]}"
    }
    function zle-line-finish ()
    {
        printf '%s' "${terminfo[rmkx]}"
    }

    zle -N zle-line-init
    zle -N zle-line-finish
fi

# git
zstyle ':vcs_info:*' enable git
zstyle ':vcs_info:git*:*' get-revision true
zstyle ':vcs_info:git*:*' check-for-changes true

setopt prompt_subst
autoload -Uz vcs_info
zstyle ':vcs_info:*' actionformats \
    '%F{5}(%f%s%F{5})%F{3}-%F{5}[%F{2}%b%F{3}|%F{1}%a%F{5}]%f '
zstyle ':vcs_info:*' formats       \
    '%F{5}(%f%s%F{5})%F{3}-%F{5}[%F{2}%b%F{5}]%f '
zstyle ':vcs_info:(sv[nk]|bzr):*' branchformat '%b%F{1}:%F{3}%r'

zstyle ':vcs_info:*' enable git cvs svn

# or use pre_cmd, see man zshcontrib
vcs_info_wrapper() {
  vcs_info
  if [ -n "$vcs_info_msg_0_" ]; then
    echo "%{$fg[blue]%}${vcs_info_msg_0_}%{$reset_color%}$del "
  fi
}
PROMPT+=$'$(vcs_info_wrapper)'
RPROMPT='%{$fg[green]%}%T%{$reset_color%} %{$fg[yellow]%}[%?]%{$reset_color%}'

# hash changes branch misc
zstyle ':vcs_info:git*' formats "(%s) %12.12i [%b%m]"
zstyle ':vcs_info:git*' actionformats "(%s|%a) %12.12i [%b%m]"
zstyle ':vcs_info:git*+set-message:*' hooks git-st git-stash

# alias
alias us='setxkbmap us'
alias fr='setxkbmap fr'
alias usfr='setxbmap us intl'

alias grep='grep --color=auto -n'
alias ls='ls --color'
alias ll='ls -l'
alias l='ll'
alias la='ls -la'
alias lla='ls -la'
alias emacs='emacs -nw'
alias e='vim'
alias reload="source ~/.zshrc"
alias irc='ssh bakablue@bakablue.fr -p 2202'
alias blog='ssh bakablue@bakablue.fr -p 2203'
alias prolo='ssh bakablue@prologin.org'
alias tree='tree -C'
alias la='ls -a'
alias ll='ls -l'

# environment variables
export CC=gcc
export CVS_RSH="ssh"
export EDITOR="vim"
export HISTFILE=~/.zsh_history
export HISTSIZE=4096
export LANG=en_US.UTF-8
export NNTPSERVER='news.epita.fr'
export SAVEHIST=4096
export TERM=rxvt-unicode
export LESSCHARSET="utf-8"
export PAGER="less"
