syntax on
set expandtab
set tabstop=4
set softtabstop=4
set shiftwidth=4
set hlsearch
set incsearch
set smarttab
set undolevels=150
set ai
set smartindent
set cindent
set autoindent
set copyindent
set rnu
set number
set textwidth=79
" Change the terminal's title
set title

colorscheme desert

set comments=sl:/*,mb:**,elx:*/
set cinoptions=(0

let g:load_doxygen_syntax=1

"curseur au meme endroit a la reouverture du fichier
if has("autocmd")
filetype plugin indent on
autocmd FileType text setlocal textwidth=78
" always jump to last edit position when opening a file
  autocmd BufReadPost *
  \ if line("'\"") > 0 && line("'\"") <= line("$") |
  \   exe "normal g`\"" |
  \ endif
endif

" indent for parenthesis in java
autocmd FileType java setlocal cinoptions+=(0

au BufRead, BufNewFile *.txt setfiletype text
au BufNewFile, BufRead *.md setfiletype markdown
" au BufNewFile, BufRead *.cl setfiletype opencl
au BufNewFile, BufRead *.l setfiletype lisp

" Column
if version >= 703
    set colorcolumn=+1
endif

" special char
set list
set listchars=trail:·,eol:¬

" Markdown syntax coloration
au BufRead,BufNewFile *.md set filetype=markdown

" === KEYBOARD MAP ===

" check English
nnoremap <C-Q> :call SetSpellEnglish()<CR>

" set mouse
nnoremap <c-m> :call togglemouse()<cr>

" map scrolling
 map <scrollwheelup> <c-y>
 map <scrollwheeldown> <c-e>

" === FUNCTIONS ===

" Enable english spelling
function SetSpellEnglish()
    if &spell
        set nospell
    else
        set spell spelllang=en_us
    endif
endfunction

" Enable mouse for people who are not used to employ vim without it.
function ToggleMouse()
    " Check if mouse is enabled
    if &mouse == 'a'
        set mouse=
    else
        set mouse=a
    endif
endfunction
